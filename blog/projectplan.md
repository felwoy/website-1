+++
title = "Projektplan"
date = "2018-03-11T00:00:00+02:00"
tags = ["Planung"]
categories = ["Projektmanagement"]
banner = "blog/images/projectplan.png"
+++

## Projektplan

Hier ist immer die aktuellste Version unseres [Projektplans](/blog/docs/projektplan.pdf) zu finden.

* [Projektplan](/blog/docs/projektplan.pdf)